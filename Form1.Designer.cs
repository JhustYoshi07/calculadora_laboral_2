﻿namespace Calculadora_Laboral_Act
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_salarioD = new System.Windows.Forms.Label();
            this.label_diaNomina = new System.Windows.Forms.Label();
            this.label_subsidio = new System.Windows.Forms.Label();
            this.label_ISR = new System.Windows.Forms.Label();
            this.label_sueldo = new System.Windows.Forms.Label();
            this.label_IMSS = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_salario = new System.Windows.Forms.TextBox();
            this.textBox_nomina = new System.Windows.Forms.TextBox();
            this.botonCalculo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_salarioD
            // 
            this.label_salarioD.AutoSize = true;
            this.label_salarioD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_salarioD.Location = new System.Drawing.Point(39, 36);
            this.label_salarioD.Name = "label_salarioD";
            this.label_salarioD.Size = new System.Drawing.Size(134, 16);
            this.label_salarioD.TabIndex = 0;
            this.label_salarioD.Text = "Ingresa tu Salario:";
            this.label_salarioD.Click += new System.EventHandler(this.label_salarioD_Click);
            // 
            // label_diaNomina
            // 
            this.label_diaNomina.AutoSize = true;
            this.label_diaNomina.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_diaNomina.Location = new System.Drawing.Point(344, 36);
            this.label_diaNomina.Name = "label_diaNomina";
            this.label_diaNomina.Size = new System.Drawing.Size(204, 16);
            this.label_diaNomina.TabIndex = 1;
            this.label_diaNomina.Text = "Ingresa los Dias de Nomina:";
            // 
            // label_subsidio
            // 
            this.label_subsidio.AutoSize = true;
            this.label_subsidio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_subsidio.Location = new System.Drawing.Point(39, 141);
            this.label_subsidio.Name = "label_subsidio";
            this.label_subsidio.Size = new System.Drawing.Size(77, 16);
            this.label_subsidio.TabIndex = 2;
            this.label_subsidio.Text = "Subsidio: ";
            // 
            // label_ISR
            // 
            this.label_ISR.AutoSize = true;
            this.label_ISR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ISR.Location = new System.Drawing.Point(344, 141);
            this.label_ISR.Name = "label_ISR";
            this.label_ISR.Size = new System.Drawing.Size(41, 16);
            this.label_ISR.TabIndex = 3;
            this.label_ISR.Text = "ISR: ";
            this.label_ISR.Click += new System.EventHandler(this.label_ISR_Click);
            // 
            // label_sueldo
            // 
            this.label_sueldo.AutoSize = true;
            this.label_sueldo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_sueldo.Location = new System.Drawing.Point(344, 237);
            this.label_sueldo.Name = "label_sueldo";
            this.label_sueldo.Size = new System.Drawing.Size(102, 16);
            this.label_sueldo.TabIndex = 4;
            this.label_sueldo.Text = "Sueldo Neto: ";
            // 
            // label_IMSS
            // 
            this.label_IMSS.AutoSize = true;
            this.label_IMSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_IMSS.Location = new System.Drawing.Point(39, 237);
            this.label_IMSS.Name = "label_IMSS";
            this.label_IMSS.Size = new System.Drawing.Size(52, 16);
            this.label_IMSS.TabIndex = 5;
            this.label_IMSS.Text = "IMSS: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 355);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Total: ";
            // 
            // textBox_salario
            // 
            this.textBox_salario.Location = new System.Drawing.Point(180, 36);
            this.textBox_salario.Name = "textBox_salario";
            this.textBox_salario.Size = new System.Drawing.Size(143, 20);
            this.textBox_salario.TabIndex = 7;
            this.textBox_salario.TextChanged += new System.EventHandler(this.textBox_salario_TextChanged);
            // 
            // textBox_nomina
            // 
            this.textBox_nomina.Location = new System.Drawing.Point(554, 36);
            this.textBox_nomina.Name = "textBox_nomina";
            this.textBox_nomina.Size = new System.Drawing.Size(143, 20);
            this.textBox_nomina.TabIndex = 8;
            this.textBox_nomina.TextChanged += new System.EventHandler(this.textBox_nomina_TextChanged);
            // 
            // botonCalculo
            // 
            this.botonCalculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botonCalculo.Location = new System.Drawing.Point(347, 349);
            this.botonCalculo.Name = "botonCalculo";
            this.botonCalculo.Size = new System.Drawing.Size(99, 29);
            this.botonCalculo.TabIndex = 9;
            this.botonCalculo.Text = "Calcular";
            this.botonCalculo.UseVisualStyleBackColor = true;
            this.botonCalculo.Click += new System.EventHandler(this.botonCalculo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.botonCalculo);
            this.Controls.Add(this.textBox_nomina);
            this.Controls.Add(this.textBox_salario);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_IMSS);
            this.Controls.Add(this.label_sueldo);
            this.Controls.Add(this.label_ISR);
            this.Controls.Add(this.label_subsidio);
            this.Controls.Add(this.label_diaNomina);
            this.Controls.Add(this.label_salarioD);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_salarioD;
        private System.Windows.Forms.Label label_diaNomina;
        private System.Windows.Forms.Label label_subsidio;
        private System.Windows.Forms.Label label_ISR;
        private System.Windows.Forms.Label label_sueldo;
        private System.Windows.Forms.Label label_IMSS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_salario;
        private System.Windows.Forms.TextBox textBox_nomina;
        private System.Windows.Forms.Button botonCalculo;
    }
}

