﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora_Laboral_Act
{
    public partial class Form1 : Form
    {

        //Jhustin Ismael Arias Perez 4-A T/M

        public Form1()
        {
            InitializeComponent();
        }

        private void label_salarioD_Click(object sender, EventArgs e)
        {

        }

        private void label_ISR_Click(object sender, EventArgs e)
        {

        }

        private void botonCalculo_Click(object sender, EventArgs e)
        {

            double dias_de_nomina= Convert.ToDouble(textBox_nomina.Text);
            double dias_de_salario = Convert.ToDouble(textBox_salario.Text);

            double percepcion = dias_de_nomina * dias_de_salario;
            //lblImporte.Text = percepcion.ToString();

            //Calculo de los datos dentro de la constante
            double Calculo_constante = 365 + Constante.vac + Constante.vacacion;
            Calculo_constante + Constante.aguinaldo;

            //Calculo del año
            Calculo_constante = Calculo_constante / 365;

            double salario_por_dia = Calculo_constante * dias_de_salario;
            double ISR, ISRTotal, pago, cuota, ISRTotal, sueldoTotal, 
            double UMA, diferencia, import, IMSS, dinero_prestado;
            double equivalente, inva, vejez, deduccion, subsidioEm;

            label_sueldo.Text = salario_por_dia.ToString();
            bool subsidio = false;
  
            if (dias_de_salario * 30 < 6000)
            {
                subsidio = true;
            }
            else
            {
                subsidio = false;
                subsidioEm = 0;
                lblSub.Text = subsidioEm.ToString();
            }

            if (salario_por_dia >= 0.01 || salario_por_dia <= 578.52)
            {
                pago = salario_por_dia - 0.01;
                pago *= 0.0192; // 1.92%
                cuota = 0;
                pago += cuota;
                ISR = cuota + pago;
                ISRTotal = salario_por_dia - ISR;
                lblISR.Text = ISRTotal.ToString();

                if ((UMA * 3) < salario_por_dia)
                {
                    diferencia = salario_por_dia - (UMA * 3);
                    import = (diferencia * dias_de_nomina) * Constante.importeP;//0.40%
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (percepcion >= 0.01 || percepcion <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 872.86 && percepcion < 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1309.21 && percepcion < 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1713.61 && percepcion < 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1745.71 && percepcion < 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 2193.76 && percepcion < 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }


                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }



                }
                else if ((UMA * 3) > salario_por_dia)
                {
                    diferencia = 0;
                    import = (diferencia * dias_de_nomina) * Constante.importeP;
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();
                }

            }
            else if (salario_por_dia >= 578.53 || salario_por_dia <= 4910.18)
            {
                pago = salario_por_dia - 578.53;
                pago *= 0.064;//6.40%
                cuota = 11.11;
                pago += cuota;
                ISR = cuota + pago;
                ISRTotal = salario_por_dia - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salario_por_dia)
                {
                    diferencia = salario_por_dia - (UMA * 3);
                    import = (diferencia * dias_de_nomina) * Constante.importeP;
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (percepcion >= 0.01 || percepcion <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 872.86 || percepcion <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1309.21 || percepcion <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1713.61 || percepcion <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1745.71 || percepcion <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 2193.76 || percepcion <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salario_por_dia)
                {
                    diferencia = 0;
                    import = (diferencia * dias_de_nomina) * Constante.importeP;
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (percepcion >= 0.01 || percepcion <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 872.86 || percepcion <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1309.21 || percepcion <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1713.61 || percepcion <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1745.71 || percepcion <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 2193.76 || percepcion <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (salario_por_dia >= 4910.19 || salario_por_dia <= 8629.20)
            {
                pago = salario_por_dia - 4910.19;
                pago *= 0.1088;//10.88%
                cuota = 288.33;
                pago += cuota;
                ISR = cuota + pago;
                ISRTotal = salario_por_dia - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salario_por_dia)
                {
                    diferencia = salario_por_dia - (UMA * 3);
                    import = (diferencia * dias_de_nomina) * Constante.importeP;//0.0040%
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (percepcion >= 0.01 || percepcion <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 872.86 || percepcion <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1309.21 || percepcion <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1713.61 || percepcion <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1745.71 || percepcion <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 2193.76 || percepcion <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salario_por_dia)
                {
                    diferencia = 0;
                    import = (diferencia * dias_de_nomina) * Constante.importeP;
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();
                    if (subsidio == true)
                    {

                        if (percepcion >= 0.01 || percepcion <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 872.86 || percepcion <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1309.21 || percepcion <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1713.61 || percepcion <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1745.71 || percepcion <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 2193.76 || percepcion <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }


            else if (salario_por_dia >= 8629.21 || salario_por_dia <= 10031.07)
            {
                pago = salario_por_dia - 8629.21;
                pago *= 0.16;//%16;
                cuota = 692.96;
                pago += cuota;
                ISR = cuota + pago;
                ISRTotal = salario_por_dia - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salario_por_dia)
                {
                    diferencia = salario_por_dia - (UMA * 3);
                    import = (diferencia * dias_de_nomina) * Constante.importeP;//0.0040%
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (percepcion >= 0.01 || percepcion <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 872.86 || percepcion <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1309.21 || percepcion <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1713.61 || percepcion <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1745.71 || percepcion <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 2193.76 || percepcion <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salario_por_dia)
                {
                    diferencia = 0;
                    import = (diferencia * dias_de_nomina) * Constante.importeP;
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (percepcion >= 0.01 || percepcion <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 872.86 || percepcion <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1309.21 || percepcion <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1713.61 || percepcion <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1745.71 || percepcion <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 2193.76 || percepcion <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                        else if (percepcion >= 3642.61)
                        {

                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }

                }
            }
            else if (salario_por_dia >= 10031.08 || salario_por_dia <= 12009.94)
            {
                pago = salario_por_dia - 10031.08;
                pago *= 0.1792;//17.92%
                cuota = 917.26;
                pago += cuota;
                ISR = cuota + pago;
                ISRTotal = salario_por_dia - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salario_por_dia)
                {
                    diferencia = salario_por_dia - (UMA * 3);
                    import = (diferencia * dias_de_nomina) * Constante.importeP;
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (percepcion >= 0.01 || percepcion <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 872.86 || percepcion <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1309.21 || percepcion <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1713.61 || percepcion <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1745.71 || percepcion <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 2193.76 || percepcion <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salario_por_dia)
                {
                    diferencia = 0;
                    import = (diferencia * dias_de_nomina) * Constante.importeP;
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();
                    if (subsidio == true)
                    {

                        if (percepcion >= 0.01 || percepcion <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 872.86 || percepcion <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1309.21 || percepcion <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1713.61 || percepcion <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1745.71 || percepcion <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 2193.76 || percepcion <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }

            }
            else if (salario_por_dia >= 12009.95 || salario_por_dia <= 24222.31)
            {
                pago = salario_por_dia - 12009.95;
                pago *= 0.2126;//21.36%
                cuota = 1271.87;
                pago += cuota;
                ISR = cuota + pago;
                ISRTotal = salario_por_dia - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salario_por_dia)
                {
                    diferencia = salario_por_dia - (UMA * 3);
                    import = (diferencia * dias_de_nomina) * Constante.importeP;
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (percepcion >= 0.01 || percepcion <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 872.86 || percepcion <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1309.21 || percepcion <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1713.61 || percepcion <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 1745.71 || percepcion <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (percepcion >= 2193.76 || percepcion <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                    }
                    else if (subsidio == false)
                    {
                        subsidioEm = 0;
                        lblSub.Text = subsidioEm.ToString();
                        sueldoTotal += subsidioEm;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > salario_por_dia)
                {
                    diferencia = 0;
                    import = (diferencia * dias_de_nomina) * Constante.importeP;
                    dinero_prestado = salario_por_dia * dias_de_nomina * Constante.dinero_prestado;
                    equivalente = salario_por_dia * dias_de_nomina * Constante.prestacion;
                    inva = salario_por_dia * dias_de_nomina * Constante.calculo_inva;
                    vejez = salario_por_dia * dias_de_nomina * Constante.calculo_vejez;
                    IMSS = diferencia + import + dinero_prestado + equivalente + inva + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    deduccion = ISRTotal + IMSS;
                    lblDeducciones.Text = deduccion.ToString();
                    sueldoTotal = percepcion - deduccion;
                    lblSueldo.Text = sueldoTotal.ToString();

                }

            }
        }

        private void textBox_nomina_TextChanged(object sender, EventArgs e)
        {
            if (Char.IsDigit(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsSeparator(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsControl(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (v.KeyChar.ToString().Equals("."))
            {
                v.Handled = false;
            }
        }

        private void textBox_salario_TextChanged(object sender, EventArgs e)
        {

            if (Char.IsDigit(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsSeparator(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsControl(v.KeyChar))
            {
                v.Handled = false;
            }
          
        }
    }
    }


}
